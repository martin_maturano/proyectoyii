<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?> Modificada</h1>

    <p>
        Esta es la página de About. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
</div>
